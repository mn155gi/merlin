# Zadanie 2 objektové programovanie

## Changelog 1.1.2019

- section 6.2 - the backpack has to have a capacity
- added _experimental_ Animation.flipAnimation

## Changelog 29.12.2018

- removed leftover methods from Actor interface - health, et c. should be implemented only by AbstractCharacter
- section 2.1 - added missing definitions for methods that were supposed to remain; getWidth & getHeight should have integer return value
- section 2.2 - SlickWorld does not implement World (but StateBasedGame, World runs inside of it), updated code accordingly
- in section 4(When you have a hammer...) void damage should have integer argument - added into the definition

__Edit: Nová verzia rieši problém s knižnicami__ &nbsp;

Zlá čarodejnica Morgana ukradla Merlinovi jeho krištáľovú guľu. (Preto žiaľ neuvidíme tragický koniec Ellen Ripley.) Ako jeden z jeho učňov sa musíš vydať na cestu do zakliateho kráľovstva a prinavrátiť Merlinovi ukradnutý artefakt.

Pozor! Merlin nanajvýš neznáša, keď sa nejaký jeho študent snaží podvádzať a je neradno si s ním zahrávať. Ak by nejaký učeň sledoval svôjho kolegu a pokúsil by sa cestou späť krištáľovú guľu ukradnúť a vydávať sa za hrdinu, majster ho nemilosrdne premení na žabu. 
(budú sa robiť testy originality, tak pozor na zhodu - neopisujte, úlohy budú formulovaná voľnejšie, takže miesto na interpretáciu bude väčšie)

## 1. Zoznám sa so svetom

Keďže Merlin kedysi so záujmom dlho sledoval project.hellen, pomocou svojej mágie pretvoril svet okolo seba na jeho podobu.

Projekt je rozdelený na dve časti:

1. to, čo by ste normálne nevideli je v balíčku engine; tento kód je určený len na čítanie, nemeňte ho, zistíme to... keď nerozumiete, prečo niečo funguje tak, ako funguje, tu to viete nájsť
2. to, čo budete programovať

---
<details>
<summary>Magic, how does it work?</summary>
Rovnako ako v prípade project-ellen, hra beží cez OpenGL, presnejšie Slick framework. Narozdiel od project-ellen, táto hra je postavená na "StateBasedGame", čo nám umožňuje mať rozličné stavy pre rôzne časti hry (svet, menu, leaderboards...)

Trieda GameLevelState v sebe implementuje samotnú hernú slučku; táto trieda je pre vás podstatná.
</details>

## 2. Actors

Podobne, ako v project-ellen, logika spočíva v tom, že všetko, čo je v hernom svete implementuje rozhranie actor.

## 2.1 AbstractActor

V balíčku sk.tuke.fei.kkui.oop.actors vytvorte abstraktnú triedu AbstractActor, ktorá implementuje rozhranie Actor - AbstractActor bude slúžiť ako základ pre vytváranie väčšiny vecí v hernom svete; **vhodne si zvoľte členské premenné** a vytvorte si dva konštruktory - jeden neparametrický a jeden s parametrom String name; pomocou mena bude následne možné identifikovať actora

_poznámka_ metódy vo všeobecnosti zodpovedajú project-ellen, niektoré sa volajú trochu inak, prípadne majú obdobnú funkcionalitu:

__Práca so svetom:__

```java
World getWorld(); //zodpovedá getScene()  
void addedToWorld(World world); //zavolaná po pridaní actora do sveta, aby sa mu nastavila referencia na herný svet  
void removeFromWorld(); //má odstrániť actora zo sveta pred vykonaním nasledujúcej iterácie hernej slučky  
boolean removedFromWorld(); //true: označuje, že herný engine má daného actora odstrániť z mapy
void setName(String name); //nastavenie mena actora, je podľa toho možné nájsť actora vo svete pomocou getWorld().getActorByName(name) - vráti prvého nájdeného s daným menom
```

__Interakcia vo svete:__

```java
int getX();
int getY(); //gettery na pozíciu actora v hernom svete  
int setPosition(int x, int y); //nastavenie pozície actora
boolean intersectsWithActor(Actor anotherActor); //zistenie kolízie s iným actorom, k zoznamu actorov sa dostanete pomocou getWorld().getActors(), prípadne getActorByName(name)
boolean isAffectedByGravity(); //keďže sa jedná o plošinovku, časť predmetov bude priťahovaná k zemi  
void setGravity(boolean isGravityEnabled); //true - Actor má padať, false - má zostať lietať
void update(); //aktualizovanie actora v rámci hernej slučky, vždy zavolané pred vykreslením, ~60x za sekundu
```

__Animácie:__

```java
void setAnimation(Animation animation); //nastavenie animácie;
Animation getAnimation(); //volá engine, to čo táto metóda vráti sa vykresľuje na obrazovku
int getWidth();
int getHeight(); //gettery na rozmery animácie - využíva sa na zistenie kolízií
```

## 2.2 To run, or not to run

v balíčku sk.tuke.fei.kkui.oop sa nachádza trieda Main. V nej si vytvorte inštanciu herného sveta;

konštruktor má nasledujúce parametre:
SlickWorld(String name, int width, int height)

```java
SlickWorld world = new SlickWorld(name, width, height);  
world.run();
```

Po úspešnom spustení by sa malo objaviť čierne okno s menu. Funkcionalita menu je už naprogramovaná.

## 2.3 Actors, actors everywhere

V balíčku sk.tuke.fei.kkui.oop.game.actors vytvorte triedu Chest ktorá rozširuje AbstractActor. Táto trieda bude následne predstavovať základný rozbiteľný predmet sveta.  
_poznámka: triedy nie sú kontrolované automatizovanými testami, takže si môžete zvoliť aj iný názov, napr. __Vase__ (a dať zodpovedajúcu animáciu) podľa toho, ako sa vám to hodí do herného sveta. Zachovajte však anglické názvy v celom projekte._

Animácia je definovaná obdobne ako v project-ellen, ale sú dva zásadné rozdiely:
_čas trvania animácie je udávaný v milisekundách_, nie sekundách, a teda je integer  
_animation.setPingPong(true)_ je potrebné volať osobitne, nie ako parameter konštruktora, ak chcete aby animácia bola prehrávaná v poradí 1 2 3 4 3 2 1 miesto 1 2 3 4 1 2 3 4  

## 3. Lets change the world!

## 3.1 Scenarios

V balíčku sk.tuke.fei.kkui.oop.game.actors si vytvorte triedu pre scenár a nech táto trieda implementuje rozhranie Scenario.  
Pomocou scenára viete implementovať vytváranie a inicializáciu actorov, nastavenie správania...; _CreateActors(World world)_ je zavolaná hneď po inicializácií herného sveta (ak je scenár nastavený)
Skúste si tu vytvoriť truhlicu a pridať ju do sveta. _Nezabudnite cez settery nastaviť X a Y súradnicu a pomocou _world.addActor(actor)_ pridať do sveta

V main pomocou _world.setScenario(scenario)_ nastavte tento scenár pred spustením herného sveta (_world.run()_)

Ak ste postupovali správne, po spustení hry by vám mala lietať niekde na mape truhlica.

## 3.2 Remember Newton

Vieme, že asi by truhlica nemala lietať, ale čo s tým vieme spraviť?
Odkedy Isaac Newton objavil gravitáciu, veci musia padať k Zemi. Vytvorte si teda v balíčku sk.tuke.fei.kkui.oop.game.commands triedu Gravity (implements Physics) ktorá nám zabezpečí, že gravitačný zákon nebude porušený (aspoň z časti).

1. _setWorld(World world)_ slúži na nastavenie sveta, aby fyzika mohla fungovať; jedná sa o jednoduchý príklad "dependency injection".
2. Zoznam actorov získate pomocou _world.getActors()_
3. Zabezpečte, aby každý actor, ktorý má padať (_isAffectedByGravity()_), padal na zem (kolíziu s podlahou viete zistiť pomocou world.intersectsWithWall(actor)). Zatiaľ postačí konštantná rýchlosť.
4. Skontrolujte implementáciu pridaním vázy do vzduchu. Nezabudnite, aby mala povolenú gravitáciu.

## 4. When you have a hammer...

...nie, nebudeme opravovať reaktory. V tejto časti vytvoríme funkcionalitu potrebnú na zničenie / poškodenie vecí a postáv v hre.

Vytvorte rozhranie Destroyable v sk.tuke.fei.kkui.oop.game.actors s nasledujúcimi metódami:

```java
void setHitPoints(int hp);
int getHitPoints();
void damage(int amountOfDamage);
void die();
```

Vytvorte triedu Health, ktorá bude reprezentovať zdravie actora, nech táto trieda obsahuje zdravie (HitPoints) zbroj (Armor) a vhodné metódy na prácu so zdravím (a podľa uváženia aj so zbrojou) (napr. _damage, heal_ a _getHP_) a konštruktor s dvoma parametrami - maxHP, armor
_poznámka:_

1. za normálnych okolností nesmie život prekročiť maxHP
2. nech armor redukuje silu útoku - stačí jednoduchý rozdiel, ale môžete si zvoliť aj sofistikovanejší výpočet

Nech Chest implementuje Destroyable a využíva Health: _maxHP = 1, armor = 0_
Zabezpečte, aby sa pri zničení zavolala die() a váza odstránila zo sveta - použite napríklad parametrický konštruktor v Health - pošlite referenciu na daného zničiteľného actora (Destroyable)

## 5. Characters!

Truhlica sa dá zničiť, nepriateľ by sa asi mal dať zabiť, čarodejník je tiež smrteľný...  
Vytvorte si v balíčku actors abstraktnú triedu _AbstractCharacter extends AbstractActor implements Destroyable_ a presuňte do nej zodpovedajúcu funkcionalitu z truhlice. Nech truhlica (Chest) rozširuje túto triedu.

## 5.1 My will is your Command

Vytvorte si v balíčku actors rozhranie Movable (extends Actor), bude sa jednať o markup interface.
Vytvorte si v balíčku commands triedu Move (implements Command), s konštruktorom

```java
public Move(Movable movable, int dx)
```

v execute() zabezpečte pohyb movable actora (nezabudnite na kolíziu so stenou!)

Obdobne si vyvorte rozhranie Jumpable (extends Actor), teraz ale potrebujeme dve metódy, ktoré nám vrátia aktuálnu výšku skoku a maximum - _int getCurrentJumpHeight(), int getMaxJumpHeight()_

ak chcete, aby actor vedel skákať aj menej, ako maximum, pridajte si _boolean isJumping()_ 
(možete vhodne upraviť aj pre dvojskok napríklad pomocou enum, nie integer, kvôli čitateľnosti)

vytvorte command Jump s konštruktorom (dy netreba, skáče sa stále hore :-) )

```java
public Jump(Jumpable jumpable)
```

v execute() implementujte skok, počítajte, že na realizáciu skoku sa execute bude volať viac krát z update() zodpovedajúceho actora. (Nezabudnite vypnúť gravitáciu pri výskoku a pri dosiahnutí maxima ju zas zapnúť)

## 5.2 Player

Vytvorte v actors triedu Player ktorá rozširuje AbstractCharacter. Nech implementuje Movable a Jumpable

(nezabudnite na vhodnú animáciu!)

v metóde update() implementujte logiku pohybu; prístup k stlačeným klávesám máte pomocou Singleton triedy Input (Input.getInstance()):

1. zistite rozdiel medzi _public boolean isKeyDown(Key key)_ a _public boolean isKeyPressed(Key key)_ k tlačidlám sa dostanete cez Input.Key.KEY_NAME
2. využite príkazy definované v predchádzajúcej podkapitole na pohyb

(keďže sa ten istý príkaz vykonáva stále pri danom pohybe, uložte si ich do členských premenných, namiesto stále nového vytvárania)

## 5.3 Attack

Vytvorte si v commands triedu:

```java
public class Attack implements Command {
    public Attack(Actor attacker, int damage) {

    }
}
```

vrámci _execute_ implementujte, nech prvý zasiahnutý Destroyable stratí X hp

1. Nech player pri stlačení napr. SPACEBAR vykoná útok (a rozbije truhlicu) (nezabudnite zodpovedajúco zmeniť animáciu počas útoku)
2. Počítajte, že útok trvá určitý čas, kým jeden neskončí, ďalší nie je možné vykonať
3. Samotný úder by nemal nastať hneď (podobne, ako pri výskoku, implementujte volanie útoku až kým neskončí), pozrite si vrámci implementácie animácie, ako by ste vedeli zistiť, kedy došlo k úderu a kedy sa animácia útoku dokončila.

## 6 Items

Všetky veci v hernom svete musia implementovať rozhranie Actor (čo hra dokáže rozpoznať a použiť), ako však zabezpečiť interakciu medzi actormi navzájom? Vytvorte rozhranie:

```java
public interface Usable<T extends Actor> {
    void use(T user);
}
```

Ak máte problém s typovými parametrami, môžete použiť:

```java
public interface Usable {
    use(Actor user);
}
```

a kontrolu robiť priamo v kóde pomocou instanceof a pretypovania, je to akceptovateľné (ale násilné) riešenie, za menej bodov.

Typový parameter v Usable nám vymedzí, kto danú vec môže použiť, napríklad iba Merlinov učeň môže použiť liečivý odvar.

Vytvorte si v balíčku items triedu

```java
public class HealingPotion extends AbstractActor implements Item, Usable<Player> {

}
```

Implementujte triedu; nech _use_ doplní 50 zdravia hráčovi
(poznámka: zobraziť správu viete pomocou metódy showMessage volanej nad objektom herného sveta)

## 6.1 Use

Vytvorte si v balíčku commands triedu:

```java
public class Use<T extends Actor> implements Command {

    public Use(Usable<T> item, T user) {

    }

}
```

(Alebo obdobne bez typových parametrov)  
Implementujte rozhranie; item je vec, ktorá sa má aplikovať na usera

V Player-ovi implementujte nech sa pri stlačení tlačítka (napr. E) použije to, na čom player stojí (ak je to možné)

(aktualizujte si projekt)

## 6.2 !Samsonite backpack 2.0

v balíčku items si vytvorte triedu Backpack (implements Inventory), táto trieda bude reprezentovať akýkoľvek batoh v hre, nielen pre hráča, napríklad aj obsah truhlice. Implementujte túto triedu, nezabudnite si vhodne zvoliť kolekciu na uchovanie predmetov.

Na zobrazenie inventáru slúži _void world.showInventory(Inventory inventory)_ (stačí zavolať raz)

_edit: nezabudnite, že batoh má mať nejakú kapacitu (definovanú v konštruktore); ak sa pokúsi niekto pridať do batoha vec, keď je plný, tak sa má vyvolať Exception - InventoryOutOfSpaceException, ktorú si zadefinujte v novom balíčku sk...game.exceptions

## 6.3 Pick it up

Vytvorte interface Keeper, ktorý bude označovať actora s inventárom. Zvážte, aké metódy má toto rozhranie obsahovať.

Vytvorte command TakeItem, ktorého cieľom bude zdvihnúť zo zeme nejaký item a pridať ho keeperovi do batoha.

Nech Player a Chest implementujú toto rozhranie.

## 7. Events

V hre sa toho môže stať veľa a ošetriť každý prípad v rámci jednej triedy môže byť náročné. Čo ak ale potrebujeme rôzne kombinácie udalostí ošetrovať dynamicky?

Využijeme Strategy pattern a rozhranie Command na dynamickú prácu s vykonávanými akciami.

Vytvorte si v commands enum CommandType ktorý bude vyjadrovať kedy sa má daná akcia vykonať.

```java
public enum CommandType {
    UPDATE, DEATH
}
```

podľa potreby si môžete následne dodefinovať ďalšie.

a pridajte si v commands rozhranie Commandable:

```java
public interface Commandable {
    void addCommand(CommandType commandType, Command command);
    void removeCommand(CommandType commandType, Command command);
}
```

addCommand bude slúžiť na pridanie akcie do zoznamu vykonávaných akcií v danej časti logiky actora. Tieto akcie by mali byť uložené v zodpovedajúcich zoznamoch, s tým, že v rámci metódy die() sa (okrem iného kódu) vykonajú všetky príkazy, ktoré boli nastavené ako CommandType.DEATH, obdobne to bude aj s UPDATE.

Nech AbstractCharacter implementuje toto rozhranie.

Vytvorte 2 nové príkazy - DropAllItems (vysype celý obsah batohu keepera na zem), RemoveFromWorld (odstráni actora z mapy) (samozrejme, obidva implementujú Command)

Zabezpečte, aby sa z truhlice vysypali veci pri jej rozbití a zmizla zo sveta.

## 7.1 Harry -- yer a wizard

__To be continued...__

Ďalšie časti budú obsahovať postavičky, veci, dvere, kúzla, triggers a vlastnú prácu