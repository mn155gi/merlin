package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.game.actors.Movable;
import sk.tuke.fei.kkui.oop.helpers.Constants;

public class Move implements Command {

    private Movable movable;
    private int dx;

    public Move(Movable movable, int dx) {
        this.movable = movable;
        this.dx = dx;
    }

    @Override
    public void execute() {
        if (dx == 1) {
            movable.setPosition(movable.getX() - Constants.STEP, movable.getY());
            if (movable.getWorld().intersectWithWall(movable)) {
                movable.setPosition(movable.getX() + Constants.STEP, movable.getY());
            }
        } else {
            movable.setPosition(movable.getX() + Constants.STEP, movable.getY());
            if (movable.getWorld().intersectWithWall(movable)) {
                movable.setPosition(movable.getX() - Constants.STEP, movable.getY());
            }
        }
    }

}
