package sk.tuke.fei.kkui.oop.game.animations;

import sk.tuke.fei.kkui.oop.engine.Animation;

public interface AnimationProvider {
    public void setAnimation(AnimationType type, Animation animation);
    public void setAnimation(AnimationType type);

    public Animation getAnimation();
    // ak by ste niekde vo vlastnej práci potrebovali, môžete si pridať:
    public Animation getAnimation(AnimationType type);

}
