package sk.tuke.fei.kkui.oop.engine.actions;

import org.newdawn.slick.state.StateBasedGame;

public class ExitGame extends AbstractMenuAction<StateBasedGame> {

    @Override
    public String getActionName() {
        return "Exit";
    }

    @Override
    public void execute(StateBasedGame stateBasedGame) {
        System.exit(0);
    }
}
