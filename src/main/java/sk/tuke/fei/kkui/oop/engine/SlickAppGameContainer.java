package sk.tuke.fei.kkui.oop.engine;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Game;
import org.newdawn.slick.SlickException;

public class SlickAppGameContainer extends AppGameContainer {

    Game game;

    public SlickAppGameContainer(Game game, int width, int height, boolean fullscreen) throws SlickException {
        super(game, width, height, fullscreen);
    }

    public void setup() throws SlickException {
        super.setup();
    }

    public void start() throws SlickException {
        try {
            getDelta();
            while (running()) {
                gameLoop();
            }
        } finally {
            destroy();
        }
        if (this.forceExit) {
            System.exit(0);
        }
    }
}