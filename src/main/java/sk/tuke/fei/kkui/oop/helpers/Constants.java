package sk.tuke.fei.kkui.oop.helpers;

import sk.tuke.fei.kkui.oop.engine.Animation;

public class Constants {

    public static final int STEP = 2;

    public static final Animation walk = new Animation("resources/sprites/wizard/walk.png",
            96, 96, 200);
    public static final Animation attack = new Animation("resources/sprites/wizard/attack.png",
            96, 96, 300);
    public static final Animation jump = new Animation("resources/sprites/wizard/jump.png",
            96, 96, 300);
    public static final Animation walkBarbarian = new Animation("resources/sprites/barbarian/walk.png",
            52, 65, 200);
    public static final Animation attackBarbarian = new Animation("resources/sprites/barbarian/attack.png",
            52, 65, 300);
    public static final Animation jumpBarbarian = new Animation("resources/sprites/barbarian/jump.png",
            52, 65, 300);
    public static final Animation walkGoblin = new Animation("resources/sprites/goblin/walk.png",
            64, 64, 200);
    public static final Animation attackGoblin = new Animation("resources/sprites/goblin/attack.png",
            64, 64, 300);
    public static final Animation jumpGoblin = new Animation("resources/sprites/goblin/jump.png",
            64, 64, 300);
    public static final Animation walkPrincess = new Animation("resources/sprites/princess/walk.png",
            41, 63, 200);
    public static final Animation attackPrincess = new Animation("resources/sprites/princess/attack.png",
            41, 63, 300);
    public static final Animation jumpPrincess = new Animation("resources/sprites/princess/jump.png",
            41, 63, 300);


    public static Animation medkit = new Animation("resources/sprites/MedKit.png", 32, 32, 0);
    public static Animation fireball = new Animation("resources/sprites/misc/fireball.png", 40, 32, 200);

    public static final int DAMAGE = 20;
}
