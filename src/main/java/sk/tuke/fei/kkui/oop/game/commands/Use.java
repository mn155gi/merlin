package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.game.actors.Actor;
import sk.tuke.fei.kkui.oop.items.Usable;

public class Use<T extends Actor> implements Command {

    private Usable<T> item;
    private T user;

    public Use(Usable<T> item, T user) {
        this.item = item;
        this.user = user;
    }

    @Override
    public void execute() {
        item.use(user);
    }
}
