package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.game.animations.AnimationType;
import sk.tuke.fei.kkui.oop.items.BackPack;

public class Chest extends AbstractCharacter implements Destroyable, Keeper {

    private BackPack backPack;

    public Chest() {
        super("chest");
        setAnimation(AnimationType.IDLE);
        getAnimation().setPingPong(true);
        health = new Health(1, 0);
        setGravity(true);
        backPack = new BackPack(1);

    }

    @Override
    public BackPack getBackPack() {
        return backPack;
    }

    @Override
    public void setAnimation(AnimationType type, Animation animation) {

    }

    @Override
    public Animation getAnimation(AnimationType type) {
        return null;
    }

    @Override
    public void die() {
        super.die();
        Coin coin = new Coin("Coin");
        coin.setPosition(this.getX(),this.getY());
        getWorld().addActor(coin);
    }
}