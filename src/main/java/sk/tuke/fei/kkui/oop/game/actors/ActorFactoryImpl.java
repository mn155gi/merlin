package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.game.factories.ActorFactory;

public class ActorFactoryImpl extends ActorFactory {

    public ActorFactoryImpl() {
    }

    @Override
    public Actor create(String actorType, String actorName) {
        switch (actorName){
            case "Merlin":
                return new Player();
            case "Coin":
                return new Coin(actorName);
            case "Chest":
                return new Chest();
        }
        return null;
    }
}
