package sk.tuke.fei.kkui.oop.items;

import sk.tuke.fei.kkui.oop.engine.Items.Inventory;
import sk.tuke.fei.kkui.oop.engine.Items.Item;
import sk.tuke.fei.kkui.oop.game.exceptions.InventoryOutOfSpaceException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BackPack implements Inventory {

    private int capacity;
    private List<Item> backPackItems;

    public BackPack(int capacity) {
        this.capacity = capacity;
        backPackItems = new ArrayList<>(this.capacity);
    }

    @Override
    public void addItem(Item item) throws InventoryOutOfSpaceException {
        if (item == null) {
            return;
        }
        if (backPackItems.size() < capacity) {
            backPackItems.add(0, item);
        } else {
            throw new InventoryOutOfSpaceException();
        }
    }

    @Override
    public void removeItem(Item item) {
        backPackItems.remove(item);
    }

    @Override
    public void removeItem(int index) {
        backPackItems.remove(index);
    }

    @Override
    public void dropAll() {
        backPackItems = new ArrayList<>(this.capacity);
    }

    @Override
    public void shiftLeft() {
        Item item = backPackItems.get(0);
        backPackItems.remove(0);
        backPackItems.add(item);
    }

    @Override
    public void shiftRight() {
        Item item = backPackItems.get(backPackItems.size() - 1);
        backPackItems.remove(item);
        backPackItems.add(0, item);
    }

    @Override
    public Iterator<Item> iterator() {
        return backPackItems.iterator();
    }
}
