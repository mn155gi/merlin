package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.game.actors.Actor;
import sk.tuke.fei.kkui.oop.game.actors.Destroyable;

public class Attack implements Command {

    private Actor attacker;
    private int damage;

    public Attack(Actor attacker, int damage) {
        this.attacker = attacker;
        this.damage = damage;
//        attacker.getAnimation().setLooping(false);
//        attacker.getAnimation().setPingPong(false);
//        attacker.setAnimation(Constants.attack);
//        attacker.getAnimation().start();
    }

    @Override
    public void execute() {
        Destroyable dealDamageToThis = null;
        for (Actor actor : attacker.getWorld().getActors()) {
            if (attacker.intersectsWithActor(actor) && actor instanceof Destroyable && actor != attacker) {
                dealDamageToThis = ((Destroyable) actor);
                break;
            }
        }
        if (dealDamageToThis != null) {
            if(((Actor)dealDamageToThis).getName().equals("Princess")){
                ((Actor) dealDamageToThis).update();
            }
            dealDamageToThis.damage(damage);
            if(!((Actor)dealDamageToThis).getName().equals("Merlin")) {
                System.out.println(((Actor) dealDamageToThis).getName() + " Health:  " + dealDamageToThis.getHitPoints());
            }
        }
    }
}
