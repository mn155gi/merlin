package sk.tuke.fei.kkui.oop.game.actors;

public interface Wizard extends Actor {

    void refillMana(int amountOfMana);

}
