package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.engine.Items.Item;
import sk.tuke.fei.kkui.oop.items.Usable;

public class Coin extends AbstractActor implements Item, Usable {

    public Coin(String name) {
        super(name);
        setAnimation(new Animation("resources/sprites/misc/coin.png",32,32,100));
    }

    @Override
    public void update() {
        /*if(getWorld().getActorByName("Merlin") != null && this.intersectsWithActor(getWorld().getActorByName("Merlin"))){
            use(this);
        }*/
    }

    @Override
    public void use(Actor user) {
        ((Player) getWorld().getActorByName("Merlin")).setNumberOfCoins(((Player) getWorld().getActorByName("Merlin")).getNumberOfCoins() + 1);
        getWorld().removeActor(this);
    }
}
