package sk.tuke.fei.kkui.oop.engine;

public interface Scenario {

    void CreateActors(World world);

}
