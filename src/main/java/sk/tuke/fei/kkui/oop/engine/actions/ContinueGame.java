package sk.tuke.fei.kkui.oop.engine.actions;

import org.newdawn.slick.state.StateBasedGame;
import sk.tuke.fei.kkui.oop.engine.SlickWorld;

public class ContinueGame extends AbstractMenuAction<StateBasedGame> {

    @Override
    public String getActionName() {
        return "Continue";
    }

    @Override
    public void execute(StateBasedGame o) {
        StateBasedGame sbg = (StateBasedGame) o;
        sbg.enterState(((SlickWorld) sbg).getCurrentGameID());
    }
}
