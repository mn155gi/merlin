package sk.tuke.fei.kkui.oop.engine;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class TiledMapFixer {

    public static void fixTiledMap(String path) {

        boolean isModified = false;

        Document doc;

        if (path.startsWith("/")) {
            path = path.substring(1);
        }

        try {
            File inputFile = new File(path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList nList = doc.getDocumentElement().getElementsByTagName("objectgroup");
            NamedNodeMap objectgroupAttributes = doc.getDocumentElement()
                    .getElementsByTagName("objectgroup").item(0).getAttributes();
            try {
                String width = objectgroupAttributes.getNamedItem("width").getNodeValue();
                String height = objectgroupAttributes.getNamedItem("height").getNodeValue();
            } catch (NullPointerException e) {
                doc.getDocumentElement().removeAttribute("renderorder");
                String mapWidth = doc.getDocumentElement().getAttribute("width");
                ((Element)doc.getDocumentElement().getElementsByTagName("objectgroup")
                        .item(0)).setAttribute("width", mapWidth);

                String mapHeight = doc.getDocumentElement().getAttribute("height");
                ((Element)doc.getDocumentElement().getElementsByTagName("objectgroup")
                        .item(0)).setAttribute("height", mapHeight);

                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                try {
                    Transformer transformer = transformerFactory.newTransformer();
                    DOMSource source = new DOMSource(doc);
                    StreamResult file = new StreamResult(new File(path));
                    transformer.transform(source, file);
                } catch (TransformerConfigurationException ex) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
