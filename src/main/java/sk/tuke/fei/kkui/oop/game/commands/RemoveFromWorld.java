package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.game.actors.Actor;

public class RemoveFromWorld implements Command {

    private Actor actor;

    public RemoveFromWorld(Actor actor) {
        this.actor = actor;
    }

    @Override
    public void execute() {
        if (actor != null && actor.getWorld() != null) {
            actor.getWorld().removeActor(actor);
        }
    }
}
