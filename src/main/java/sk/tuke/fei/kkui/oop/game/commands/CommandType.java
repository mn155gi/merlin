package sk.tuke.fei.kkui.oop.game.commands;

public enum CommandType {
    UPDATE, DEATH, HIT
}
