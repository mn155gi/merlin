package sk.tuke.fei.kkui.oop.game.actors;

public class Health {

    private int maxHP;
    private int armor;
    private int HP;

    public Health(int maxHP, int armor) {
        this.maxHP = maxHP;
        this.armor = armor;
    }

    public void damage(int amountOfDamage) {
        if (armor != 0) {
            if (armor - amountOfDamage >= 0) {
                armor = armor - amountOfDamage;
                return;
            } else {
                amountOfDamage = armor - amountOfDamage;
                armor = 0;
            }
        }
        if (HP - amountOfDamage > 0) {
            HP = HP - amountOfDamage;
        } else if (HP - amountOfDamage <= 0) {
            HP = 0;
        }
    }

    public void heal(int amountOfHP) {
        if (HP  + amountOfHP < maxHP) {
            HP = amountOfHP + HP;
        } else {
            HP = maxHP;
        }
    }

    public int getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

}
