package sk.tuke.fei.kkui.oop.engine.actions;

import org.newdawn.slick.state.StateBasedGame;

public class OptionsOpen extends AbstractMenuAction<StateBasedGame> {
    @Override
    public String getActionName() {
        return "Options";
    }

    @Override
    public void execute(StateBasedGame stateBasedGame) {
        stateBasedGame.enterState(1);
    }
}
