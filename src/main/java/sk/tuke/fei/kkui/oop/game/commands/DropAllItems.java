package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.game.actors.Keeper;

public class DropAllItems implements Command {

    private Keeper keeper;

    public DropAllItems(Keeper keeper) {
        this.keeper = keeper;
    }

    @Override
    public void execute() {
        if (keeper != null && keeper.getBackPack() != null) {
            keeper.getBackPack().dropAll();
        }
    }
}
