package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.engine.Message;
import sk.tuke.fei.kkui.oop.game.animations.AnimationType;
import sk.tuke.fei.kkui.oop.game.commands.Attack;

import java.awt.*;

public class Barbarian extends AbstractCharacter implements Movable {

    public Barbarian(String name) {
        super(name);
        setAnimation(AnimationType.IDLE);
        health = new Health(5,4);
        setHitPoints(5);
    }

    @Override
    public void update() {
        //Message message = new Message("Conan's Health:  " + getHealth().getHP() + " Armor:  " + getHealth().getArmor(), 450, 65, Color.GREEN);
        //getWorld().showMessage(message);
        if(getWorld().getActorByName("Merlin") != null && this.intersectsWithActor(getWorld().getActorByName("Merlin"))){
            new Attack(this, 1).execute();

        }


    }

    @Override
    public void setAnimation(AnimationType type, Animation animation) {

    }

    @Override
    public Animation getAnimation(AnimationType type) {
        return null;
    }
}
