package sk.tuke.fei.kkui.oop.engine;

import org.newdawn.slick.GameContainer;

public class Input {

    private static Input instance = null;
    private GameContainer container;

    public static enum Key {

        SPACE(57),
        ENTER(28),
        LEFT(203),
        RIGHT(205),
        UP(200),
        DOWN(208),
        ESCAPE(1),
        A(30), B(48), C(46), D(32), E(18),
        F(33), G(34), H(35), I(23), J(36),
        K(37), L(38), M(50), N(49), O(24),
        P(25), Q(16), R(19), S(31), T(20),
        U(22), V(47), W(17), Z(44),
        END(207),
        HOME(199),
        DELETE(211),
        INSERT(210),
        RETURN(28),
        PAUSE(197),
        F1(59), F2(60), F3(61), F4(62), F5(63), F6(64),
        F7(65), F8(66), F9(67), F10(68), F11(87), F12(88),
        BACK(14),
        MINUS(12), MULTIPLY(55), DIVIDE(181), ADD(78),
        TAB(15);

        private final int value;

        private Key(int value) {
            this.value = value;
        }
    }

    private Input() {

    }

    Input(GameContainer container) {
        this.container = container;
        instance = this;
    }

    public static Input getInstance() { //no multi-threading in play - no need for thread safety
        if (instance == null) {
            instance = new Input();
        }
        return instance;
    }

    public boolean isKeyDown(Key key) {
        return this.container.getInput().isKeyDown(key.value);
    }

    public boolean isKeyPressed(Key key) {
        return this.container.getInput().isKeyPressed(key.value);
    }
}
