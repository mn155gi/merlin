package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.game.actors.Jumpable;

public class Jump implements Command {

    private Jumpable jumpable;
    private int numberOfCalls = 0;

    public Jump(Jumpable jumpable) {
        this.jumpable = jumpable;
    }

    @Override
    public void execute() {
        jumpable.setGravity(false);
        this.numberOfCalls++;
        jumpable.setPosition(jumpable.getX(), jumpable.getY() - 1);
        while (jumpable.getCurrentJumpHeight() != jumpable.getMaxJumpHeight()) {
            jumpable.setPosition(jumpable.getX(), jumpable.getY() - 1);
            jumpable.setCurrentJumpHeight(jumpable.getCurrentJumpHeight() + 1);

        }
        jumpable.setCurrentJumpHeight(0);
        jumpable.setGravity(true);
    }


}
