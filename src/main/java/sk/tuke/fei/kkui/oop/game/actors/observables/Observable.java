package sk.tuke.fei.kkui.oop.game.actors.observables;

public interface Observable {
    void addObserver(Observer observer);
    void removeObserver(Observer observer);

    void notifyObservers();

}
