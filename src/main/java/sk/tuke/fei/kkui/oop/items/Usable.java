package sk.tuke.fei.kkui.oop.items;

import sk.tuke.fei.kkui.oop.game.actors.Actor;

public interface Usable<T extends Actor> {

    void use(T user);

}
