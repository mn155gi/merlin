package sk.tuke.fei.kkui.oop.engine.actions;

import org.newdawn.slick.state.StateBasedGame;

public class OptionsBack extends AbstractMenuAction<StateBasedGame> {
    @Override
    public String getActionName() {
        return "Back";
    }

    //TODO: add dynamic state handling
    @Override
    public void execute(StateBasedGame stateBasedGame) {
        stateBasedGame.enterState(0);
    }
}
