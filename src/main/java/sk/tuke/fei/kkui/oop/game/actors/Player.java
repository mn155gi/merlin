package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.engine.Input;
import sk.tuke.fei.kkui.oop.engine.Message;
import sk.tuke.fei.kkui.oop.game.animations.AnimationType;
import sk.tuke.fei.kkui.oop.game.commands.Attack;
import sk.tuke.fei.kkui.oop.game.commands.Jump;
import sk.tuke.fei.kkui.oop.game.commands.Move;
import sk.tuke.fei.kkui.oop.game.commands.Use;
import sk.tuke.fei.kkui.oop.helpers.Constants;
import sk.tuke.fei.kkui.oop.items.BackPack;
import sk.tuke.fei.kkui.oop.items.Usable;

import java.awt.*;

public class Player extends AbstractCharacter implements Movable, Jumpable, Keeper, Wizard {

    private BackPack backPack;
    private int maxJumpHeight;
    private int currentJumpHeight;

    public int getNumberOfCoins() {
        return numberOfCoins;
    }

    public void setNumberOfCoins(int numberOfCoins) {
        this.numberOfCoins = numberOfCoins;
    }

    private int numberOfCoins;

    Player() {
        super("Merlin");
        setAnimation(Constants.walk);
        getAnimation().setPingPong(true);
        getAnimation().stop();
        health = new Health(200, 0);
        setHitPoints(200);
        mana = new Mana(100, 100);
        maxJumpHeight = 50;
        currentJumpHeight = 0;
        setGravity(true);
        backPack = new BackPack(7);
        setNumberOfCoins(0);
    }

    @Override
    public void update() {
        Input input = Input.getInstance();
        if (input.isKeyPressed(Input.Key.LEFT) || input.isKeyDown(Input.Key.LEFT)) {
            new Move(this, 1).execute();
            this.setAnimation(AnimationType.MOVE_LEFT);
        } else if (input.isKeyPressed(Input.Key.RIGHT) || input.isKeyDown(Input.Key.RIGHT)) {
            new Move(this, 0).execute();
            this.setAnimation(AnimationType.MOVE);
        } else if (input.isKeyPressed(Input.Key.UP)) {
            new Jump(this).execute();
            this.setAnimation(AnimationType.JUMP);
        } else if (input.isKeyPressed(Input.Key.SPACE)) {
            new Attack(this, 1).execute();
        } else if(input.isKeyPressed(Input.Key.L)){
            new Projectile(this.getName(),1);
        }else if (input.isKeyPressed(Input.Key.E)) {
            for (Actor actor : getWorld().getActors()) {
                if (intersectsWithActor(actor) && actor instanceof Usable) {
                    new Use((Usable) actor, this).execute();
                }
            }
        }else{
            this.setAnimation(AnimationType.IDLE);
        }
        Message message = new Message("Health:  " + getHealth().getHP() + " Mana: " + mana.getAmountOfMana() + " Coins:" + getNumberOfCoins(), 450, 25, Color.GREEN);
        getWorld().showMessage(message);
    }

    @Override
    public int getCurrentJumpHeight() {
        return currentJumpHeight;
    }

    @Override
    public void setCurrentJumpHeight(int currentJumpHeight) {
        this.currentJumpHeight = currentJumpHeight;
    }

    @Override
    public int getMaxJumpHeight() {
        return maxJumpHeight;
    }

    @Override
    public BackPack getBackPack() {
        return backPack;
    }


    @Override
    public void refillMana(int amountOfMana) {
        mana.addMana(amountOfMana);
    }

    @Override
    public void setAnimation(AnimationType type, Animation animation) {

    }

    @Override
    public Animation getAnimation(AnimationType type) {
        return null;
    }
}
