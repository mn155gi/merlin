package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.engine.Items.Item;
import sk.tuke.fei.kkui.oop.game.actors.Keeper;
import sk.tuke.fei.kkui.oop.game.exceptions.InventoryOutOfSpaceException;

public class TakeItem implements Command {

    private Keeper keeper;
    private Item item;

    public TakeItem(Keeper keeper, Item item) {
        this.keeper = keeper;
        this.item = item;
    }

    @Override
    public void execute() {
        if (keeper.getBackPack() == null) {
            return;
        }
        try {
            keeper.getBackPack().addItem(item);
            keeper.getWorld().removeActor(item);
        } catch (InventoryOutOfSpaceException e) {
            e.printStackTrace();
        }
    }
}
