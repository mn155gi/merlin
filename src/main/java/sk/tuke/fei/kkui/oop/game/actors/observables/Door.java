package sk.tuke.fei.kkui.oop.game.actors.observables;

import sk.tuke.fei.kkui.oop.game.actors.AbstractActor;

public class Door extends AbstractActor implements Observer {

    public Door(String name) {
        super(name);
    }

    @Override
    public void update() {

    }

    @Override
    public void notify(Object sender) {

    }
}
