package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.engine.Physics;
import sk.tuke.fei.kkui.oop.engine.World;
import sk.tuke.fei.kkui.oop.game.actors.Actor;

public class Gravity implements Physics {

    private World world;

    @Override
    public void execute() {
        for (Actor actor : world.getActors()) {
            if (actor.isAffectedByGravity()) {
                while (!world.intersectWithWall(actor)) {
                    actor.setPosition(actor.getX(), actor.getY() + 1);
                    if (world.intersectWithWall(actor)) {
                        actor.setPosition(actor.getX(), actor.getY() - 1);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void setWorld(World world) {
        this.world = world;
    }
}
