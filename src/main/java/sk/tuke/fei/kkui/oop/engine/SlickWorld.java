package sk.tuke.fei.kkui.oop.engine;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.GameState;
import org.newdawn.slick.state.StateBasedGame;
import sk.tuke.fei.kkui.oop.engine.Items.Inventory;
import sk.tuke.fei.kkui.oop.engine.actions.*;
import sk.tuke.fei.kkui.oop.engine.exceptions.GameException;
import sk.tuke.fei.kkui.oop.engine.states.AbstractState;
import sk.tuke.fei.kkui.oop.engine.states.GameLevelState;
import sk.tuke.fei.kkui.oop.engine.states.MenuState;
import sk.tuke.fei.kkui.oop.game.actors.Actor;

import java.util.Iterator;

public class SlickWorld extends StateBasedGame {

    public static int stateID = 1;
    private SlickAppGameContainer container;
    private Input input;

    private NewGame newGame;
    private Factory factory;
    private Scenario scenario;
    private Physics physics;
    private String mapPath;

    private int currentGameStateID = 1;

    public SlickWorld(String title, int width, int height) {
        super(title);
        System.setProperty("org.lwjgl.opengl.Display.allowSoftwareOpenGL", "true");
        try {
            this.container = new SlickAppGameContainer(this, width, height, false);
            this.container.setShowFPS(true);
            this.container.setTargetFrameRate(Integer.parseInt((String) Options.getProperties().get("fps")));
            this.container.setVSync(Boolean.parseBoolean((String)Options.getProperties().getProperty("vsync")));
            this.container.setup();
            this.container.setAlwaysRender(true);

        } catch (SlickException ex) {
            throw new GameException(ex);
        }

        input = new Input(container);
        //input = Input.getInstance();

    }

    public void addActor(Actor actor) {
        GameState state = getCurrentState();

        if (state instanceof World) {
            ((World)state).addActor(actor);
        } else {
            ((World)getState(stateID)).addActor(actor);
        }

    }

    public void setFactory(Factory factory) {
        this.factory = factory;
        newGame.setFactory(factory);
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
        newGame.setScenario(scenario);
    }

    public void setPhysics(Physics physics) {
        this.physics = physics;
        newGame.setPhysics(physics);
    }

    public void setMap(String path) {
        this.mapPath = path;
        newGame.setMap(mapPath);

    }

    public void removeActor(Actor actor) {
        GameState state = getCurrentState();
        if (state instanceof World) {
            ((World)state).removeActor(actor);
        }
        else {
            ((World)getState(stateID)).removeActor(actor);
        }
    }

    public final void run() {
        try {
            this.container.start();
        } catch (Exception ex) {
            throw new GameException(ex);
        }
    }

    public Iterator<Actor> iterator() {
        GameState state = getCurrentState();
        if (state instanceof GameLevelState) {
            return ((GameLevelState)state).iterator();
        }
        return ((World)getState(stateID)).iterator();
    }

    public void showMessage(Message message) {
        GameState state = getCurrentState();
        ((AbstractState)state).showMessage(message);
    }

    public void showBackpack(Inventory inventory) {
        GameState state = getCurrentState();
        if (state instanceof GameLevelState) {
            ((GameLevelState)state).showInventory(inventory);
        }
    }

    @Override
    public void initStatesList(GameContainer gc) throws SlickException {
        newGame = new NewGame();
        MenuState menu = new MenuState();
        menu.addAction(new ContinueGame());
        menu.addAction(newGame);
        menu.addAction(new OptionsOpen());
        menu.addAction(new ExitGame());

        MenuState options = new MenuState();
        options.addAction(new OptionsDummyAction());
        options.addAction(new OptionsBack());

        addState(menu);
        addState(options);

    }


    public int getCurrentGameID() {
        return currentGameStateID;
    }

    public void setCurrentGameID(int currentGameID) {
        this.currentGameStateID = currentGameID;
    }
}
