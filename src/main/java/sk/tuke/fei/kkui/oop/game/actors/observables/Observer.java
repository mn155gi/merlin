package sk.tuke.fei.kkui.oop.game.actors.observables;

public interface Observer<T> {
    void notify(T sender);
}
