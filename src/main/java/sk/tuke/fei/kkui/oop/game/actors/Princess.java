package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.engine.Message;
import sk.tuke.fei.kkui.oop.game.animations.AnimationType;

import java.awt.*;

public class Princess extends AbstractCharacter implements Movable{

    private int counter = 0;
    private boolean counterboolean = false;

    public Princess(String name) {
        super(name);
        setAnimation(AnimationType.IDLE);
    }

    @Override
    public void update() {
        for(Actor actor:getWorld().getActors()){
            if(this.intersectsWithActor(actor)&& actor instanceof Player){
                getWorld().showMessage(new Message("You win", 400, 400, Color.RED));
                counterboolean = true;

            }
        }
        if(counterboolean){
            counter++;
            if(counter == 50){
                System.exit(0);
            }
        }
    }

    @Override
    public void setAnimation(AnimationType type, Animation animation) {

    }

    @Override
    public Animation getAnimation(AnimationType type) {
        return null;
    }
}
