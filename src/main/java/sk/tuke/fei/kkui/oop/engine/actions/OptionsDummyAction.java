package sk.tuke.fei.kkui.oop.engine.actions;

import org.newdawn.slick.state.StateBasedGame;

public class OptionsDummyAction extends AbstractMenuAction<StateBasedGame> {
    @Override
    public String getActionName() {
        return "Dummy action";
    }

    @Override
    public void execute(StateBasedGame stateBasedGame) {
        System.out.print("It works!\n");
    }
}
