package sk.tuke.fei.kkui.oop.game.actors;

public interface Destroyable {

    void setHitPoints(int hp);
    int getHitPoints();
    void damage(int amountOfDamage);
    void die();

}
