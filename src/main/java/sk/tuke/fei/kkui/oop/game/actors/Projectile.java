package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.game.commands.*;
import sk.tuke.fei.kkui.oop.helpers.Constants;

import java.util.ArrayList;
import java.util.List;

import static sk.tuke.fei.kkui.oop.helpers.Constants.DAMAGE;

public class Projectile extends AbstractActor implements Commandable, Movable {

    private List<Command> deathList;
    private List<Command> updateList;

    public Projectile(String name, int idx) {
        super(name);
        setAnimation(Constants.fireball);
        setGravity(false);
        deathList = new ArrayList<>();
        updateList = new ArrayList<>();
        addCommand(CommandType.HIT, new Attack(this, DAMAGE));
        addCommand(CommandType.UPDATE, new Move(this, idx));
    }

    @Override
    public void update() {
        if (getWorld().intersectWithWall(this)) {
            getWorld().removeActor(this);
           return;
        }
       boolean destroyProjectile = false;
       for (Actor actor : getWorld().getActors()) {
           if (this.intersectsWithActor(actor)) {
               if (actor instanceof Commandable) {
                   ((Commandable) actor).addCommand(CommandType.HIT, new Attack(this, DAMAGE));
                   destroyProjectile = true;
                   break;
               }
           }
       }
       if (destroyProjectile) {
           getWorld().removeActor(this);
       }
    }

    @Override
    public void addCommand(CommandType commandType, Command command) {
        if (commandType == CommandType.DEATH) {
            deathList.add(command);
        } else if (commandType == CommandType.UPDATE) {
            updateList.add(command);
        }
    }

    @Override
    public void removeCommand(CommandType commandType, Command command) {
        if (commandType == CommandType.DEATH && deathList != null && !deathList.isEmpty()) {
            deathList.remove(command);
        } else if (commandType == CommandType.UPDATE && updateList != null && !updateList.isEmpty()) {
            updateList.remove(command);
        }
    }


}
