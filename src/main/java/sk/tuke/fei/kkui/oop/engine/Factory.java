package sk.tuke.fei.kkui.oop.engine;

import sk.tuke.fei.kkui.oop.game.actors.Actor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

public interface Factory {

    Actor create(String actorType, String actorName);
}
