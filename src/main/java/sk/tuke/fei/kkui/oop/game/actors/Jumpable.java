package sk.tuke.fei.kkui.oop.game.actors;

public interface Jumpable extends Actor {

    int getCurrentJumpHeight();
    void setCurrentJumpHeight(int jumpHeight);
    int getMaxJumpHeight();

}
