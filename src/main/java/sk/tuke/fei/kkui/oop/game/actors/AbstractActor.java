package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.engine.World;

public abstract class AbstractActor implements Actor {

    private String name;
    private Animation animation;
    private int posX;
    private int posY;
    private World world;
    private boolean affectedByGravity;
    private boolean removedFromWorld;

    public AbstractActor(String name) {
        this.name = name;
    }

    //nastavenie mena actora, je podľa toho možné nájsť actora vo svete pomocou getWorld().getActorByName(name) - vráti prvého nájdeného s daným menom
    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    //volá engine, to čo táto metóda vráti sa vykresľuje na obrazovku
    @Override
    public Animation getAnimation() {
        return animation;
    }

    //nastavenie animácie;
    @Override
    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    //nastavenie pozície actora
    @Override
    public void setPosition(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
    }

    //gettery na pozíciu actora v hernom svete
    @Override
    public int getX() {
        return posX;
    }

    @Override
    public int getY() {
        return posY;
    }


    public void setPosX(int posX) {
        this.posX = posX;
    }

    //gettery na rozmery animácie - využíva sa na zistenie kolízií
    @Override
    public int getWidth() {
        return getAnimation().getWidth();
    }

    @Override
    public int getHeight() {
        return getAnimation().getHeight();
    }


    //zodpovedá getScene()
    @Override
    public World getWorld() {
        return world;
    }

    //aktualizovanie actora v rámci hernej slučky, vždy zavolané pred vykreslením, ~60x za sekundu
    @Override
    public abstract void update();

    //zavolaná po pridaní actora do sveta, aby sa mu nastavila referencia na herný svet
    @Override
    public void addedToWorld(World world) {
        this.world = world;
    }

    //zistenie kolízie s iným actorom, k zoznamu actorov sa dostanete pomocou getWorld().getActors(), prípadne getActorByName(name)
    @Override
    public boolean intersectsWithActor(Actor actor) {
        return (actor.getX() + actor.getWidth() >= this.posX)
                && (actor.getY() + actor.getHeight() >= this.posY)
                && (this.posX + this.getWidth() >= actor.getX())
                && (this.posY + this.getHeight() >= actor.getY());
    }

    //keďže sa jedná o plošinovku, časť predmetov bude priťahovaná k zemi
    @Override
    public boolean isAffectedByGravity() {
        return affectedByGravity;
    }

    //true - Actor má padať, false - má zostať lietať
    @Override
    public void setGravity(boolean isGravityEnabled) {
        affectedByGravity = isGravityEnabled;
    }

    //má odstrániť actora zo sveta pred vykonaním nasledujúcej iterácie hernej slučky
    @Override
    public void removeFromWorld() {
        if(!removedFromWorld) {
            removedFromWorld = true;
            getWorld().removeActor(this);
        }
    }

    //true: označuje, že herný engine má daného actora odstrániť z mapy
    @Override
    public boolean removedFromWorld() {
        return removedFromWorld;
    }
}
