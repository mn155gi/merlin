package sk.tuke.fei.kkui.oop.items;

import sk.tuke.fei.kkui.oop.engine.Items.Item;
import sk.tuke.fei.kkui.oop.game.actors.AbstractActor;
import sk.tuke.fei.kkui.oop.game.actors.Player;
import sk.tuke.fei.kkui.oop.helpers.Constants;

public class HealingPotion extends AbstractActor implements Item, Usable<Player> {

    public HealingPotion(String name) {
        super(name);
        setAnimation(Constants.medkit);
    }

    @Override
    public void update() {}

    @Override
    public void use(Player user) {
        user.getHealth().heal(50);
    }
}
