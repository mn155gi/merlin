package sk.tuke.fei.kkui.oop.engine;

import sk.tuke.fei.kkui.oop.engine.states.GameLevelState;
import sk.tuke.fei.kkui.oop.game.actors.Actor;
import sk.tuke.fei.kkui.oop.game.commands.Command;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public interface Physics extends Command {

    @Override
    void execute();
//    void addActor(Actor actor);
//    void removeActor(Actor actor);
    void setWorld(World world);
}
