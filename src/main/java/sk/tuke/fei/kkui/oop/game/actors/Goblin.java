package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.game.animations.AnimationType;

public class Goblin extends AbstractCharacter implements Movable {

    public Goblin(String name) {
        super(name);
        setAnimation(AnimationType.IDLE);
    }

    @Override
    public void setAnimation(AnimationType type, Animation animation) {

    }

    @Override
    public Animation getAnimation(AnimationType type) {
        return null;
    }
}
