package sk.tuke.fei.kkui.oop.game.commands;

import sk.tuke.fei.kkui.oop.game.actors.Wizard;

public class RefillMana implements Command {

    private Wizard wizard;

    public RefillMana(Wizard wizard) {
        this.wizard = wizard;
    }

    @Override
    public void execute() {
        if (wizard != null) {
            wizard.refillMana(1);
        }
    }
}
