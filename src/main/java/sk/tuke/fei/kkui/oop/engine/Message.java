package sk.tuke.fei.kkui.oop.engine;

import java.awt.Color;

public class Message {

    private final String text;
    private final int x;
    private final int y;
    private final Color color;

    public Message(String text, int x, int y, Color color) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public Message(String text, int x, int y) {
        this.text = text;
        this.x = x;
        this.y = y;
        this.color = Color.WHITE;
    }

    public String getText() {
        return this.text;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public Color getColor() {
        return this.color;
    }
}

