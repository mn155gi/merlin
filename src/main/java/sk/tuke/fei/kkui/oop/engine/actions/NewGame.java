package sk.tuke.fei.kkui.oop.engine.actions;

import org.newdawn.slick.state.StateBasedGame;
import sk.tuke.fei.kkui.oop.engine.Factory;
import sk.tuke.fei.kkui.oop.engine.Physics;
import sk.tuke.fei.kkui.oop.engine.Scenario;
import sk.tuke.fei.kkui.oop.engine.states.GameLevelState;

public class NewGame extends AbstractMenuAction<StateBasedGame> {

    private Factory factory;
    private Scenario scenario;
    private Physics physics;
    private String mapPath = "resources/maps/basicTest.tmx";

    @Override
    public String getActionName() {
        return "New Game";
    }

    public void setFactory(Factory factory) {
        this.factory = factory;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }
    public void setPhysics(Physics physics) {
        this.physics = physics;
    }
    public void setMap(String path) {
        mapPath = path;
    }

    @Override
    public void execute(StateBasedGame stateBasedGame) {
        GameLevelState gameLevelState = new GameLevelState();
        int x = gameLevelState.getID();
        stateBasedGame.addState(gameLevelState);
        gameLevelState.setFactory(factory);
        gameLevelState.setScenario(scenario);
        gameLevelState.setGameLevelPhysics(physics);
        gameLevelState.setMap(mapPath);
        stateBasedGame.enterState(x);
    }
}
