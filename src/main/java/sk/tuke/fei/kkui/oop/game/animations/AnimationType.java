package sk.tuke.fei.kkui.oop.game.animations;

public enum AnimationType {
    IDLE, MOVE, ATTACK, HIT, JUMP, DEATH, //môžete si pridať _RIGHT k názvu
    IDLE_LEFT, MOVE_LEFT, ATTACK_LEFT, HIT_LEFT, JUMP_LEFT, DEATH_LEFT;
    }
