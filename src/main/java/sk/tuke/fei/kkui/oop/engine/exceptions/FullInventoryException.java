package sk.tuke.fei.kkui.oop.engine.exceptions;

public class FullInventoryException extends RuntimeException {

    public FullInventoryException(String s) {
        super(s);
    }

}
