package sk.tuke.fei.kkui.oop.engine;

import sk.tuke.fei.kkui.oop.engine.Items.Inventory;
import sk.tuke.fei.kkui.oop.game.actors.Actor;

import java.util.Iterator;
import java.util.List;

public interface World {

    void addActor(Actor actor);
    void removeActor(Actor actor);
    Iterator<Actor> iterator();
    boolean intersectWithWall(Actor actor);
    List<Actor> getActors();
    void showMessage(Message message);
//    boolean isWall(int x, int y);
    void setWall(int x, int y, boolean wall);
    Actor getActorByName(String name);

    int getTileWidth();
    int getTileHeight();

    void showInventory(Inventory inventory);
}