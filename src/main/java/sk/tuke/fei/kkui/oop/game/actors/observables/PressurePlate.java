package sk.tuke.fei.kkui.oop.game.actors.observables;

public class PressurePlate implements Observable {
    @Override
    public void addObserver(Observer observer) {

    }

    @Override
    public void removeObserver(Observer observer) {

    }

    @Override
    public void notifyObservers() {

    }
}
