package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.items.BackPack;

public interface Keeper extends Actor {

    BackPack getBackPack();

}
