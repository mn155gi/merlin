package sk.tuke.fei.kkui.oop.engine.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import sk.tuke.fei.kkui.oop.engine.Input;
import sk.tuke.fei.kkui.oop.engine.Message;
import sk.tuke.fei.kkui.oop.engine.actions.Action;

import java.util.ArrayList;
import java.util.List;

public class MenuState extends AbstractState {

    private int ID;

    private List<Action<StateBasedGame>> menuActions = new ArrayList<>();
    private int index = 1;
    private StateBasedGame sbg;
    private Message message;

    public MenuState() {
        super();
        this.ID = stateID++;
    }

    @Override
    public void showMessage(Message message) {
        this.message = message;
    }

    public void addAction(Action<StateBasedGame> parameter) {
        menuActions.add(parameter);

    }

    @Override
    public int getID() {
        return this.ID;
    }

    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
        this.sbg = sbg;
    }

    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics grphcs) throws SlickException {
//        grphcs.drawString("Current game state: " + String.valueOf(sbg.getCurrentStateID()), 10, 30);
        for (int i = 0; i < menuActions.size(); i++) {
            grphcs.drawString(((Action) menuActions.get(i)).getActionName(), 300, (i + 1) * 50);
        }
        grphcs.drawRect(250, (index + 1) * 50 - 20, 200, 50);
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int i) throws SlickException {
        Input input = Input.getInstance();

        if (input.isKeyPressed(Input.Key.ENTER)) {
            ((Action) menuActions.get(index)).execute(sbg);
        }
        if (input.isKeyPressed(Input.Key.DOWN)) {
            if (index >= menuActions.size() - 1) {
                index = 0;
            } else {
                index++;
            }
        } else if (input.isKeyPressed(Input.Key.UP)) {
            if (index <= 0) {
                index = menuActions.size() - 1;
            } else {
                index--;
            }
        }
    }
}
