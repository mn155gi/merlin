package sk.tuke.fei.kkui.oop.engine;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

public class Animation implements Serializable {

    private transient org.newdawn.slick.Animation slickAnimation;
    private String resource;
    private int width;
    private int height;
    private int duration;
    private int rotation;
    private boolean looping = true;
    private boolean pingPong;

    public boolean isFlipped() {
        return isFlipped;
    }

    private boolean isFlipped = false;

    public Animation(String resource, int width, int height, int duration) {
        this.resource = resource;
        this.width = width;
        this.height = height;
        this.duration = duration;
    }

    public Animation(String resource, int width, int height) {
        this(resource, width, height, 10);
    }

    public String getResource() {
        return this.resource;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public int getDuration() {
        return this.duration;
    }

    public int getRotation() {
        return this.rotation;
    }

    public void draw(int x, int y) {
        getSlickAnimation().draw(x, y);
    }

    public void draw(int x, int y, int width, int height) {
        getSlickAnimation().draw(x, y, width, height);
    }

    private org.newdawn.slick.Animation getSlickAnimation() {
        if (this.slickAnimation == null) {
            this.slickAnimation = AnimationCache.getInstance().loadAnimation(this.resource, this.width, this.height, this.duration);
            getSlickAnimation().setPingPong(this.pingPong);
            getSlickAnimation().setLooping(this.looping);
        }
        return this.slickAnimation;
    }

    public void stop() {
        getSlickAnimation();
        if (this.slickAnimation != null) {
            getSlickAnimation().stop();
        }
    }

    public void start() {
        if (this.slickAnimation != null) {
            getSlickAnimation().start();
        }
    }

    public void setPingPong(boolean pingPong) {
        this.pingPong = pingPong;
        if (this.slickAnimation != null) {
            this.slickAnimation.setPingPong(pingPong);
        }
    }

    public void setLooping(boolean looping) {
        this.looping = looping;
        if (this.slickAnimation != null) {
            this.slickAnimation.setLooping(looping);
        }
    }

    public void stopAt(int frameIndex) {
        if (this.slickAnimation != null) {
            this.slickAnimation.stopAt(frameIndex);
        }
    }

    public void setCurrentFrame(int frameIndex) {
        if (this.slickAnimation != null) {
            this.slickAnimation.setCurrentFrame(frameIndex);
        }
    }

    public int getCurrentFrame() {
        if (this.slickAnimation != null) {
            return this.slickAnimation.getFrame();
        }
        return -1;
    }

    public int getFrameCount() {
        if (this.slickAnimation != null) {
            return this.slickAnimation.getFrameCount();
        }
        return -1;
    }

    public void setDuration(int duration) {
        this.duration = duration;
        if (this.slickAnimation != null) {
            for (int i = 0; i < this.slickAnimation.getFrameCount(); i++) {
                this.slickAnimation.setDuration(i, duration);
            }
        }
    }

    public void setRotation(int angle) {
        this.rotation = (angle % 360);
        org.newdawn.slick.Animation newAnimation = new org.newdawn.slick.Animation();
        for (int i = 0; i < getSlickAnimation().getFrameCount(); i++) {
            Image image = this.slickAnimation.getImage(i);
            image.setRotation(angle);
            newAnimation.addFrame(image, this.slickAnimation.getDuration(i));
        }
        newAnimation.setLooping(this.looping);
        newAnimation.setPingPong(this.pingPong);
        setDuration(this.duration);

        this.slickAnimation = newAnimation;
    }

    private static class AnimationCache {

        private static AnimationCache instance = new AnimationCache();
        private Map<String, SpriteSheet> cachedSpritesSheet = new HashMap<String, SpriteSheet>();

        static AnimationCache getInstance() {
            return instance;
        }

        SpriteSheet loadSpriteSheet(String resource, int tileWidth, int tileHeight) {
            try {
                SpriteSheet spriteSheet = (SpriteSheet) this.cachedSpritesSheet.get(resource);
                if (spriteSheet == null) {
                    spriteSheet = new SpriteSheet(resource, tileWidth, tileHeight);
                    this.cachedSpritesSheet.put(resource, spriteSheet);
                }
                return spriteSheet;
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }

        org.newdawn.slick.Animation loadAnimation(String resource, int tileWidth, int tileHeight, int duration) {
            return new org.newdawn.slick.Animation(loadSpriteSheet(resource, tileWidth, tileHeight), duration);
        }
    }

    public void flipAnimation() {
        org.newdawn.slick.Animation slickAnimation = new org.newdawn.slick.Animation();
        org.newdawn.slick.Animation oldAnimation = getSlickAnimation();
        for (int i = 0; i < oldAnimation.getFrameCount(); i++) {
            slickAnimation.addFrame(oldAnimation.getImage(i).getFlippedCopy(true, false), duration);
        }

        if(isFlipped){
            isFlipped = false;
        }else {
            isFlipped = true;
        }
        this.slickAnimation = slickAnimation;
        slickAnimation.restart();
        slickAnimation.setCurrentFrame(oldAnimation.getFrame());
        slickAnimation.update(0);

    }
}