package sk.tuke.fei.kkui.oop.game.actors.observables;

import sk.tuke.fei.kkui.oop.game.actors.Actor;
import sk.tuke.fei.kkui.oop.items.Usable;

public class Lever implements Observable, Usable {
    @Override
    public void addObserver(Observer observer) {

    }

    @Override
    public void removeObserver(Observer observer) {

    }

    @Override
    public void notifyObservers() {

    }

    @Override
    public void use(Actor user) {

    }
}
