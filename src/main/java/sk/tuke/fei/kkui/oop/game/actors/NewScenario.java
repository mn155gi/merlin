package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Scenario;
import sk.tuke.fei.kkui.oop.engine.World;
import sk.tuke.fei.kkui.oop.game.commands.CommandType;
import sk.tuke.fei.kkui.oop.game.commands.Gravity;
import sk.tuke.fei.kkui.oop.game.commands.RefillMana;

import javax.swing.text.Position;

public class NewScenario implements Scenario {

    @Override
    public void CreateActors(World world) {
        Chest chest = new Chest();
        chest.setPosition(35,35);
        Coin coin = new Coin("Coin1");
        coin.setPosition(500,500);
        coin.setGravity(false);
        Player player = new Player();
        player.addCommand(CommandType.UPDATE, new RefillMana(player));
        player.setPosition(105, 35);
        Barbarian conan = new Barbarian("Conan");
        conan.setPosition(550,500);
        conan.setGravity(true);
        Princess princess = new Princess("Princess");
        princess.setPosition(650,500);
        princess.setGravity(true);
        Goblin goblin = new Goblin("Princess");
        goblin.setPosition(35,500);
        goblin.setGravity(true);
        world.addActor(chest);
        world.addActor(goblin);
        world.addActor(princess);
        world.addActor(coin);
        world.addActor(player);
        world.addActor(conan);
    }
}
