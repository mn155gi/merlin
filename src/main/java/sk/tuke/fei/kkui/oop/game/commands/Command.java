package sk.tuke.fei.kkui.oop.game.commands;

public interface Command {
    void execute();
}
