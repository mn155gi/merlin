package sk.tuke.fei.kkui.oop.game.commands;

public interface Commandable {

    void addCommand(CommandType commandType, Command command);
    void removeCommand(CommandType commandType, Command command);

}
