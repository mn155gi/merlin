package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.engine.World;

public interface Actor {
    void setName(String name);
    void setAnimation(Animation animation);
    void setPosition(int posX, int posY);
    String getName();
    int getX();
    int getY();
    int getWidth();
    int getHeight();
    Animation getAnimation();
    World getWorld();
    void update();
    void addedToWorld(World world);
    boolean intersectsWithActor(Actor actor);
    boolean isAffectedByGravity();
    void setGravity(boolean isGravityEnabled);
    void removeFromWorld();
    boolean removedFromWorld();
}
