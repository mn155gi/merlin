package sk.tuke.fei.kkui.oop.engine.states;

import org.newdawn.slick.state.BasicGameState;
import sk.tuke.fei.kkui.oop.engine.Input;
import sk.tuke.fei.kkui.oop.engine.Message;

public abstract class AbstractState extends BasicGameState {

    private static Input input;
    public static int stateID = 0;

    public abstract void showMessage(Message message);
    public void setInput(Input input)  {
        AbstractState.input = input;
    }
    public Input getInput() {
        if (input == null) {

        }
        return input;
    }

}
