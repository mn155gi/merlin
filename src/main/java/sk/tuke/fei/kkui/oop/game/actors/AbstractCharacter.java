package sk.tuke.fei.kkui.oop.game.actors;

import sk.tuke.fei.kkui.oop.engine.Animation;
import sk.tuke.fei.kkui.oop.game.animations.AnimationProvider;
import sk.tuke.fei.kkui.oop.game.animations.AnimationType;
import sk.tuke.fei.kkui.oop.game.commands.*;
import sk.tuke.fei.kkui.oop.game.exceptions.UnsupportedCommandTypeException;

import java.util.ArrayList;
import java.util.List;

import static sk.tuke.fei.kkui.oop.helpers.Constants.*;

public abstract class AbstractCharacter extends AbstractActor implements Destroyable, Commandable, AnimationProvider {

    Health health;
    Mana mana;
    private int seconds;
    private List<Command> deathList;
    private List<Command> updateList;

    public AbstractCharacter(String name) {
        super(name);
    }

    public Health getHealth() {
        return health;
    }

    @Override
    public void update() {
        if (updateList != null) {
            for (Command command : updateList) {
                if (command instanceof RefillMana) {
                    if (this instanceof Wizard) {
                        seconds++;
                        if (seconds >= 60) {
                            command.execute();
                            seconds = 0;
                        }
                    } else {
                        try {
                            throw new UnsupportedCommandTypeException();
                        } catch (UnsupportedCommandTypeException e) {
                            e.printStackTrace();
                        }
                    }
                } else if (command instanceof Move) {
                    if (this instanceof Movable) {
                        command.execute();
                    } else {
                        try {
                            throw new UnsupportedCommandTypeException();
                        } catch (UnsupportedCommandTypeException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        if (deathList != null) {
            for (Command command : deathList) {
                command.execute();
            }
        }
        setPosition(getX(), getY());
    }

    @Override
    public int getHitPoints() {
        return health.getHP();
    }

    @Override
    public void setHitPoints(int hp) {
        health.setHP(hp);
    }

    @Override
    public void damage(int amountOfDamage) {
        health.damage(amountOfDamage);
        if (health.getHP() <= 0) {
            executeCommands();
            die();
        }
    }

    @Override
    public void die() {
        removeFromWorld();
    }

    @Override
    public void addCommand(CommandType commandType, Command command) {
        if (commandType == CommandType.DEATH) {
            if (deathList == null) {
                deathList = new ArrayList<>();
            }
            deathList.add(command);
        } else if (commandType == CommandType.UPDATE) {
            if (updateList == null) {
                updateList = new ArrayList<>();
            }
            updateList.add(command);
        }
    }

    @Override
    public void removeCommand(CommandType commandType, Command command) {
        if (commandType == CommandType.DEATH && deathList != null) {
            deathList.remove(command);
        } else if (commandType == CommandType.UPDATE && updateList != null) {
            updateList.remove(command);
        }
    }

    private void executeCommands() {
        if (deathList != null) {
            for (Command command : deathList) {
                command.execute();
            }
        }
        if (updateList != null) {
            for (Command command : updateList) {
                command.execute();
            }
        }
    }

    public void setAnimation(AnimationType type) {
        if (this instanceof Chest) {
            switch (type){
                case IDLE:
                    this.setAnimation(new Animation("resources/sprites/misc/treasure_chest.png", 90, 48, 500));
                    break;
                case DEATH:
                    this.removeFromWorld();
                    break;
            }

        } else if (this instanceof Player) {
            switch (type) {
                case IDLE:
                    this.setAnimation(walk);
                    this.getAnimation().stop();
                    break;
                case ATTACK:
                    this.setAnimation(attack);
                    break;
                case JUMP:
                    this.setAnimation(jump);
                    break;
                case IDLE_LEFT:
                    this.setAnimation(walk);
                    if (!this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.getAnimation().stop();
                    break;
                case MOVE:
                    if (this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.setAnimation(walk);
                    break;
                case MOVE_LEFT:
                    if (!this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.setAnimation(walk);
                    break;

            }
        }
        else if (this instanceof Barbarian) {
            switch (type) {
                case IDLE:
                    this.setAnimation(walkBarbarian);
                    this.getAnimation().stop();
                    break;
                case ATTACK:
                    this.setAnimation(attackBarbarian);
                    break;
                case JUMP:
                    this.setAnimation(jumpBarbarian);
                    break;
                case IDLE_LEFT:
                    this.setAnimation(walkBarbarian);
                    if (!this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.getAnimation().stop();
                    break;
                case MOVE:
                    if (this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.setAnimation(walkBarbarian);
                    break;
                case MOVE_LEFT:
                    if (!this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.setAnimation(walkBarbarian);
                    break;

            }
        }
        else if (this instanceof Goblin) {
            switch (type) {
                case IDLE:
                    this.setAnimation(walkGoblin);
                    this.getAnimation().stop();
                    break;
                case ATTACK:
                    this.setAnimation(attackGoblin);
                    break;
                case JUMP:
                    this.setAnimation(jumpGoblin);
                    break;
                case IDLE_LEFT:
                    this.setAnimation(walkGoblin);
                    if (!this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.getAnimation().stop();
                    break;
                case MOVE:
                    if (this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.setAnimation(walkGoblin);
                    break;
                case MOVE_LEFT:
                    if (!this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.setAnimation(walkGoblin);
                    break;

            }
        }
        else if (this instanceof Princess) {
            switch (type) {
                case IDLE:
                    this.setAnimation(walkPrincess);
                    this.getAnimation().stop();
                    break;
                case ATTACK:
                    this.setAnimation(attackPrincess);
                    break;
                case JUMP:
                    this.setAnimation(jumpPrincess);
                    break;
                case IDLE_LEFT:
                    this.setAnimation(walkPrincess);
                    if (!this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.getAnimation().stop();
                    break;
                case MOVE:
                    if (this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.setAnimation(walkPrincess);
                    break;
                case MOVE_LEFT:
                    if (!this.getAnimation().isFlipped()) {
                        this.getAnimation().flipAnimation();
                    }
                    this.setAnimation(walkPrincess);
                    break;

            }
        }

    }

    public void setAnimationProvider(AnimationProvider animationProvider) {

    }


}
