package sk.tuke.fei.kkui.oop;

import sk.tuke.fei.kkui.oop.engine.Options;
import sk.tuke.fei.kkui.oop.engine.SlickWorld;
import sk.tuke.fei.kkui.oop.engine.World;
import sk.tuke.fei.kkui.oop.game.actors.ActorFactoryImpl;
import sk.tuke.fei.kkui.oop.game.actors.NewScenario;
import sk.tuke.fei.kkui.oop.game.actors.Player;
import sk.tuke.fei.kkui.oop.game.commands.Gravity;

import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties cfg = Options.getProperties();
        SlickWorld world = new SlickWorld("Remorseless winter", 800, 600);
        world.setScenario(new NewScenario());
        Gravity gravity = new Gravity();
        ActorFactoryImpl actorFactory = new ActorFactoryImpl();
        world.setPhysics(gravity);
        world.setFactory(actorFactory);
        world.run();

    }
}
