package sk.tuke.fei.kkui.oop.game.actors.observables;

import sk.tuke.fei.kkui.oop.engine.Items.Item;
import sk.tuke.fei.kkui.oop.game.actors.AbstractActor;

public class Ball extends AbstractActor implements Item {
    public Ball(String name) {
        super(name);
    }

    @Override
    public void update() {

    }
}
