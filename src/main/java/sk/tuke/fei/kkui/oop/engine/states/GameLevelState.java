package sk.tuke.fei.kkui.oop.engine.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tiled.TiledMap;
import sk.tuke.fei.kkui.oop.engine.*;
import sk.tuke.fei.kkui.oop.engine.Items.Inventory;
import sk.tuke.fei.kkui.oop.engine.Items.Item;
import sk.tuke.fei.kkui.oop.engine.exceptions.GameException;
import sk.tuke.fei.kkui.oop.game.actors.Actor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GameLevelState extends AbstractState implements Serializable, World {

    private static final String WALL_LAYER = "walls";
    private static final String BACKGROUND_LAYER = "background";
    private static final int BORDER = 2;
    private String mapResource = null;
    private TiledMap tiledMap = null;
    private Factory factory = null;
    private Scenario scenario = null;
    private List<Actor> actors = new ArrayList<>();
    private List<Actor> actorsToAdd = new ArrayList<>();
    private List<Actor> triggers = new ArrayList<>();
    private Actor centeredOn;
    private Message message;
    private Inventory inventory;
    private boolean debugGraphics = false;
    private Class<? extends Actor>[] renderOrder;
    private SlickWorld slickWorld;
    private Physics gameLevelPhysics;
    private int ID;

    public GameLevelState(SlickWorld slickWorld) {
        this();
        this.slickWorld = slickWorld;

    }

    public GameLevelState() {
        ID = stateID++;
    }

    @Override
    public int getID() {
        return ID;
    }

    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {

    }

    public void setMap(String resource) {
        this.mapResource = resource;
        loadMap();
    }

    protected void loadMap() {
        try {
            this.actors.clear();
            TiledMapFixer.fixTiledMap(this.mapResource);
            this.tiledMap = new TiledMap(this.mapResource);

            loadActors();
            for (Actor actor : this.actors) {
                actor.addedToWorld(this);
            }
        } catch (SlickException ex) {
            throw new GameException(ex);
        }
    }

    protected void loadActors() throws GameException {
        for (int groupID = 0; groupID < this.tiledMap.getObjectGroupCount(); groupID++) {
            for (int objectID = 0; objectID < this.tiledMap.getObjectCount(groupID); objectID++) {
                String type = this.tiledMap.getObjectType(groupID, objectID);
                String name = this.tiledMap.getObjectName(groupID, objectID);
                if (this.factory != null) {
                    Actor actor = this.factory.create(type, name);
                    if (actor != null) {
                        actor.setPosition(this.tiledMap.getObjectX(groupID, objectID), this.tiledMap.getObjectY(groupID, objectID));
                        addActor(actor);
                    }
                }

            }
        }

        if (this.scenario != null) {
            scenario.CreateActors(this);
        }
    }

    public void setFactory(Factory factory) {
        this.factory = factory;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public void setGameLevelPhysics(Physics physics) {
        this.gameLevelPhysics = physics;
        if (physics != null) {
            physics.setWorld(this);
        }
    }

    public void centerOn(Actor actor) {
        this.centeredOn = actor;
    }

//    @Override
    private boolean isWall(int x, int y) {
        if (this.tiledMap == null) {
            return false;
        }
        if ((x >= 0) && (y >= 0) && (x < this.tiledMap.getWidth()) && (y < this.tiledMap.getHeight())) {
            int index = this.tiledMap.getTileId(x, y, this.tiledMap.getLayerIndex("walls"));
            return index != 0;
        }
        return true;
    }

    @Override
    public void setWall(int x, int y, boolean wall) {
        if ((this.tiledMap != null)
                && (x >= 0) && (y >= 0) && (x < this.tiledMap.getWidth()) && (y < this.tiledMap.getHeight())) {
            if (wall) {
                this.tiledMap.setTileId(x, y, this.tiledMap.getLayerIndex("walls"), 1);
            } else {
                this.tiledMap.setTileId(x, y, this.tiledMap.getLayerIndex("walls"), 0);
            }
        }
    }

    @Override
    public boolean intersectWithWall(Actor actor) {
        if (this.tiledMap == null) {
            return false;
        }
        int tileWidth = this.tiledMap.getTileWidth();
        int tileHeight = this.tiledMap.getTileHeight();

        int tileXStart = (actor.getX() + 2) / tileWidth;
        int tileXEnd = (actor.getX() + actor.getWidth() - 4) / tileWidth;
        int tileYStart = (actor.getY() + 2) / tileHeight;
        int tileYEnd = (actor.getY() + actor.getHeight() - 4) / tileHeight;
        for (int tileX = tileXStart; tileX <= tileXEnd; tileX++) {
            for (int tileY = tileYStart; tileY <= tileYEnd; tileY++) {
                if (isWall(tileX, tileY)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void addActor(Actor actor) {
        actorsToAdd.add(actor);
    }

    private void addActors() {
        if (!actorsToAdd.isEmpty()) {
            actorsToAdd.forEach(actor -> {
                this.actors.add(actor);
                actor.addedToWorld(this);
            });
            actorsToAdd = new ArrayList<>();
        }
    }


    @Override
    public void removeActor(Actor actor) {
        actor.removeFromWorld();
    }

    @Override
    public Iterator<Actor> iterator() {
        return this.actors.iterator();
    }

    @Override
    public void showMessage(Message message) {
        this.message = message;
    }

    @Override
    public void showInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    private int getXOffset(GameContainer gc) {
        if (this.centeredOn != null) {
            return (gc.getWidth() - this.centeredOn.getWidth()) / 2 - this.centeredOn.getX();
        }
        return 0;
    }

    private int getYOffset(GameContainer gc) {
        if (this.centeredOn != null) {
            return (gc.getHeight() - this.centeredOn.getHeight()) / 2 - this.centeredOn.getY();
        }
        return 0;
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int i) throws SlickException {
        if (gc.getInput().isKeyPressed(1)) {
            gc.getInput().clearControlPressedRecord();
            ((SlickWorld) sbg).setCurrentGameID(this.ID);
            sbg.enterState(0);
        }

        addActors();
        List<Actor> listOfActors = new ArrayList<>();

        triggers.forEach(trigger -> trigger.update());

        Iterator<Actor> iterator = actors.iterator();
        Actor actor;
        while (iterator.hasNext()) {
            actor = iterator.next();
            actor.update();
            if (actor.removedFromWorld()) {
            } else {
                listOfActors.add(actor);
            }
        }
        actors = listOfActors;
        if (gameLevelPhysics != null) {
            gameLevelPhysics.execute();
        }
    }

    public void setRenderOrder(Class<? extends Actor>... actorClasses) {
        this.renderOrder = actorClasses;
    }

    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics graphics) throws SlickException {
        graphics.drawString("Current game state: " + String.valueOf(ID), 10, 30);
        if (this.centeredOn != null) {
            graphics.translate(getXOffset(gc), getYOffset(gc));
        }
        renderMap(graphics);
        if (this.debugGraphics) {
            graphics.setColor(org.newdawn.slick.Color.green);

            int width = this.tiledMap.getWidth();
            int height = this.tiledMap.getHeight();
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (isWall(x, y)) {
                        graphics.fillRect(x * this.tiledMap.getTileWidth(), y * this.tiledMap.getTileHeight(), this.tiledMap.getTileWidth(), this.tiledMap.getTileHeight());
                    }
                }
            }
        }
        renderActors(this.actors, graphics);
        renderActors(this.triggers, graphics);
        if (this.centeredOn != null) {
            graphics.translate(-getXOffset(gc), -getYOffset(gc));
        }
        renderInventory(gc, graphics);

        renderMessage(graphics);
    }

    private void renderMap(Graphics graphics) {
        if (this.tiledMap != null) {
            this.tiledMap.render(0, 0, this.tiledMap.getLayerIndex("background"));
        }
    }

    @SuppressWarnings("unchecked")
    private void renderActors(List<Actor> listOfActors, Graphics graphics) {
        if (this.renderOrder == null) {
            for (Actor actor : listOfActors) {
                Animation animation = actor.getAnimation();
                if (animation != null) {
                    animation.draw(actor.getX(), actor.getY());
                } else {
                    throw new IllegalStateException("Animation missing for" + actor.getClass().getName());
                }
            }
            if (this.centeredOn != null) {
                this.centeredOn.getAnimation().draw(this.centeredOn.getX(), this.centeredOn.getY());
            }
        } else {
            for (Class actorClass : this.renderOrder) {
                renderActorsOfType(graphics, actorClass);
            }
        }
    }

    private void renderActorsOfType(Graphics graphics, Class<? extends Actor> type) {
        for (Actor actor : this.actors) {
            if (type.isInstance(actor)) {
                Animation animation = actor.getAnimation();
                if (animation != null) {
                    animation.draw(actor.getX(), actor.getY());
                } else {
                    throw new IllegalStateException("Animation missing for" + actor.getClass().getName());
                }
            }
        }
    }

    private void renderInventory(GameContainer gc, Graphics graphics) {
        int x;
        if (this.inventory != null) {
            x = 4;
            Color color = graphics.getColor();
            graphics.setColor(org.newdawn.slick.Color.black);
            graphics.fillRect(0.0F, gc.getHeight() - 24, gc.getWidth(), gc.getHeight());
            for (Item item : this.inventory) {
                item.getAnimation().draw(x, gc.getHeight() - 20, 16, 16);
                x += item.getWidth() + 4;
            }

            graphics.setColor(color);
        }
    }

    private void renderMessage(Graphics graphics) {
        if (this.message != null) {
            org.newdawn.slick.Color color = new org.newdawn.slick.Color(this.message.getColor().getRGB());
            graphics.setColor(color);
            graphics.drawString(this.message.getText(), this.message.getX(), this.message.getY());
        }
    }

    @Override
    public List<Actor> getActors() {
        return actors;
    }

    @Override
    public Actor getActorByName(String name) {
        return actors.stream().filter(actor -> actor.getName().equals(name)).findFirst().orElse(null);
    }

    @Override
    public int getTileWidth() {
        return this.tiledMap.getTileWidth();
    }

    @Override
    public int getTileHeight() {
        return this.tiledMap.getTileHeight();
    }
}
