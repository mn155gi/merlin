package sk.tuke.fei.kkui.oop.engine.actions;

public interface Action<T> {

    public String getActionName();

    public void execute(T t);

}
