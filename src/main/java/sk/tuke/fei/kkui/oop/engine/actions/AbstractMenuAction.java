package sk.tuke.fei.kkui.oop.engine.actions;

import org.newdawn.slick.state.StateBasedGame;

public abstract class AbstractMenuAction<T extends StateBasedGame> implements Action<T> {

    @Override
    public String getActionName() {
        return "Nothing";
    }

    @Override
    public void execute(T t) {

    }
}
