package sk.tuke.fei.kkui.oop.game.actors;

public class Mana {

    public int getAmountOfMana() {
        return amountOfMana;
    }

    private int amountOfMana;
    private int maxAmountOfMana;

    public Mana(int amountOfMana, int maxAmountOfMana) {
        this.amountOfMana = amountOfMana;
        this.maxAmountOfMana = maxAmountOfMana;
    }

    public void drainMana(int amountOfMana) {
        if (this.amountOfMana - amountOfMana <= 0) {
            this.amountOfMana = 0;
        } else {
            this.amountOfMana -= amountOfMana;
        }
    }

    public void addMana(int amountOfMana) {
        if (this.amountOfMana + amountOfMana < maxAmountOfMana) {
            this.amountOfMana += amountOfMana;
        } else {
            this.amountOfMana = this.maxAmountOfMana;
        }
    }
}
