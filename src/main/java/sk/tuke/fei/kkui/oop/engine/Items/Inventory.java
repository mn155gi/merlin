package sk.tuke.fei.kkui.oop.engine.Items;

import sk.tuke.fei.kkui.oop.game.exceptions.InventoryOutOfSpaceException;

import java.util.Iterator;

public interface Inventory extends Iterable<Item> {
    void addItem(Item item) throws InventoryOutOfSpaceException;

    void removeItem(Item item);

    void removeItem(int index);

    void dropAll();

    void shiftLeft();

    void shiftRight();

    @Override
    Iterator<Item> iterator();

}
